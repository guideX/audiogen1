• Audiogen •
Thanks for trying Audiogen. Team Nexgen is extremely interested in your feedback about Audiogen. E-mail any bug/ideas/complaints to guide_X@live.com

• System Requirements •
- IBM Compatible PC
- Audiogen has been tested on Windows 98, Windows 98 SE, Windows XP Pro/Home 2600 and Windows XP Pro/Home 2600 sp1 and should function proporly on those systems. It probably also works on Windows 2000, Windows ME, and Windows NT. Windows 95 and Windows 3.X and below is not supported
- 200 MHZ Processor Required. (Does not run on 166 and below).
- 128 MB RAM
- 640 MB free disc space recomended, 100 MB free disc space reqired
- Speakers/Sound card
- CD Burner or CD drive is required

• Whats new •
- Added taskbar icon
- Added playlist view
- Added always on top setting
- Fixed additional bugs
- Systray features

• Existing Features •
- Tag editor
- Release history window
- Settings dialog
- Rip cd audio
- Encode .wav to .mp3
- Encode .wav to .wma
- Make mp3 albums
- Make audio cd's out of .wav files
- Convert .mp3 files to .wav files
- Convert .wma files to .wav files
- Normalize .wav files
- Search through playlists
- Remember all media files found
- Playback *.qt;*.mov;*.dat;*.snd;*.mpg;*.mpa;*.mpv;*.enc;*.m1v;*.mp2;*.mp3;*.mpe;*.mpeg;*.mpm;*.au;*.snd;*.aif;*.aiff;*.aifc;*.wav;*.wmv;*.wma files
- Skinable
- Automatic Bitrate Converter
- Adjust volume
- Select playback channel
- Scriptability (uses the vbscript launguage)
- Forward/backward button
- Audio information
- On board search
- Error handling system
- Progress bar can scan through media easily and accuratly
- Drive changer
- Bitrate changer
- Right clicking treeviews prompts for search in that directory
- Media file information area. Shows bitrate, title, format, and length
- Much more

• Support •
- http://www.team-nexgen.com/contact.shtml

• Credits •
- Programming: Leon J Aiossa (|guideX|)
- Graphics: Colin Foss (KnightFal)

• Contact •
- guide_X@live.com

• (c) Copyright 2003-2004 Developed by Team Nexgen
